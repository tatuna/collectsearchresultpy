from bs4 import BeautifulSoup
import pymongo
import requests
from flask import Flask, render_template, request
from bson.json_util import loads, dumps
from threading import Thread
from selenium import webdriver

driver = webdriver.Chrome(executable_path=r'./chromedriver.exe')
db_server = pymongo.MongoClient("mongodb://studio-1:27017/")
db = db_server["com_parser"]
sites = db["sites"]

app = Flask(__name__)

driver.get("https://on.ge/story/50768-%E1%83%92%E1%83%90%E1%83%98%E1%83%AA%E1%83%90%E1%83%9C%E1%83%98%E1%83%97-%E1%83%A5%E1%83%90%E1%83%A0%E1%83%97%E1%83%95%E1%83%94%E1%83%9A%E1%83%98-%E1%83%98%E1%83%A3%E1%83%9C%E1%83%99%E1%83%94%E1%83%A0%E1%83%94%E1%83%91%E1%83%98-%E1%83%A0%E1%83%9D%E1%83%9B%E1%83%9A%E1%83%94%E1%83%91%E1%83%9B%E1%83%90%E1%83%AA-%E1%83%93%E1%83%90%E1%83%9B%E1%83%9D%E1%83%A3%E1%83%99%E1%83%98%E1%83%93%E1%83%94%E1%83%91%E1%83%9A%E1%83%9D%E1%83%91%E1%83%98%E1%83%A1%E1%83%97%E1%83%95%E1%83%98%E1%83%A1-%E1%83%91%E1%83%A0%E1%83%AB%E1%83%9D%E1%83%9A%E1%83%90%E1%83%A1-%E1%83%A8%E1%83%94%E1%83%A1%E1%83%AC%E1%83%98%E1%83%A0%E1%83%94%E1%83%A1-%E1%83%97%E1%83%90%E1%83%95%E1%83%98")
print(driver.page_source)


@app.route('/')
def index():
   return render_template('index.html')


@app.route('/search/<query>')
def search(query):
    get_searched_data(query)
    return dumps(sites.find({ "search_word": query }))


@app.route('/all_sites')
def all_sites():
    db_sites = sites.find()
    page = int(request.args['pageNumber'])

    return dumps(db_sites[20*(page-1):20*page])

@app.route('/all_sites/with_comments')
def all_sites_comm():
    db_sites = sites.find({"has_comment": True})
    page = int(request.args['pageNumber'])
    return dumps(db_sites[20*(page-1):20*page])

@app.route('/all_sites/comm_len')
def all_sites_len_comm():
    db_sites = sites.find({"has_comment": True})
    return dumps(db_sites.count())


@app.route('/all_sites/len')
def all_sites_len():
    db_sites = sites.find()
    return dumps(db_sites.count())


@app.route('/all_sites/check')
def check_comment():
    thread = Thread(target=detect_fb_comments)
    thread.start()
    return 'started'



# def detect_fb_selenium(link=""):
#     check_sites = sites.find({ "parsed": False })
#     for ch_sits in check_sites:
#         # print(ch_sits)
#         try:
#             driver.get(ch_sits["link"])
#             soup = BeautifulSoup(driver.page_source, "html.parser")
#             driver.close()
#             comments = soup.find_all("iframe")
#             print(comments)
#             for com in comments:
#                 comment = com.attrs
#                 if "https://www.facebook.com/" in comment and "comments.php" in comment:
#                     print(comment)
#                     sites.update_one({"link":ch_sits["link"]}, {"$set": {"has_comment":True}})
#             sites.update_one({"link":ch_sits["link"]}, {"$set": {"parsed":True}})
#         except:
#             print("Someting Went Wrong on Comments")



            




def get_searched_data(url=""):
    source_code = requests.get('https://www.google.com/search?q=' + url)
    plain_text = source_code.text
    soup = BeautifulSoup(plain_text, "html.parser")

    # Get Web Details
    details = soup.select('.ZINbbc.xpd.O9g5cc.uUPGi > .kCrYT:first-child')
    descriptions = soup.select('.ZINbbc.xpd.O9g5cc.uUPGi > .kCrYT:last-child .BNeawe.s3v9rd.AP7Wnd .BNeawe.s3v9rd.AP7Wnd')
    for i in range(0, len(descriptions)):
        try:    
            heading = details[i].select_one('.BNeawe.vvjwJb.AP7Wnd').string
            src = details[i].select_one('.BNeawe.UPmit.AP7Wnd').string
            link = details[i].find('a').attrs['href'][7:]
            description  = descriptions[i].string
            sites.insert_one({
                "heading":heading,
                "link":link,
                "src":src,
                "description":description,
                "parsed":False,
                "has_comment": False,
                "search_word":url,
                "com_len": 0
            })
        except:
            print("Some Problem In Searching")






def detect_fb_comments():
    check_sites = sites.find({ "parsed": False })
    for ch_sits in check_sites:
        # print(ch_sits)
        try:
            source_code = requests.get(ch_sits["link"])
            plain_text = source_code.text
            soup = BeautifulSoup(plain_text, "html.parser")
            comments = soup.find_all("iframe")
            print(comments)
            for com in comments:
                comment = com.attrs
                if "https://www.facebook.com/" in comment and "comments.php" in comment:
                    print(comment)
                    sites.update_one({"link":ch_sits["link"]}, {"$set": {"has_comment":True}})
            sites.update_one({"link":ch_sits["link"]}, {"$set": {"parsed":True}})
        except:
            print("Someting Went Wrong on Comments")




if __name__ == '__main__':
   app.run(debug = True)